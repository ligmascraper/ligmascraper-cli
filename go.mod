module cli

go 1.22.2

require (
	github.com/manifoldco/promptui v0.9.0
	gitlab.com/ligmascraper/ligma v0.1.1
)

require (
	github.com/antchfx/htmlquery v1.3.1 // indirect
	github.com/antchfx/xpath v1.3.0 // indirect
	github.com/chzyer/readline v1.5.1 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/tidwall/gjson v1.17.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/h2non/gentleman.v2 v2.0.5 // indirect
)
