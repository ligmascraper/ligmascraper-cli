package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/ligmascraper/ligma"
	"os"
	"strings"
	"sync"
)

var jsonOutput = flag.Bool("j", false, "Print output as a JSON file")
var searchMode = flag.Bool("S", false, "Search for a string")
var scrapeMode = flag.Bool("c", false, "Scrape a provided URL")
var scrapeFirstResultMode = flag.Bool("f", false, "Perform search and scrape first result")

func main() {
	flag.Parse()

	if *searchMode {
		if len(os.Args) == 1 {
			fmt.Println("You need to provide a query!")
			return
		}
		performSearch()
	} else if *scrapeMode {
		if len(os.Args) == 1 {
			fmt.Println("You need to provide a target URL!")
			return
		}
		performScrape()
	} else if *scrapeFirstResultMode {
		if len(os.Args) == 1 {
			fmt.Println("You need to provide a search query!")
			return
		}
		performFirstResultScrape()
	} else {
		interActiveMode()
	}

}

func performFirstResultScrape() {
	searchResult := ligma.Search(strings.Join(flag.Args(), " "))

	if searchResult.SetsCount == 0 && !*jsonOutput {
		fmt.Println("No search results found!")
		return
	}

	result := ligma.Scrape(searchResult.Chemicals[0].Products[0].Url)

	if *jsonOutput {
		jsonString, _ := json.Marshal(result)
		fmt.Print(string(jsonString))
		return
	}

	printResults(result)
}

func interActiveMode() {
	query := promptSearchQuery()

	searchResults := ligma.Search(query)

	chemical := selectChemical(searchResults)

	product := selectProduct(chemical)

	chemData := ligma.Scrape(product.Url)

	printResults(chemData)
}

func selectProduct(chem ligma.Chemical) ligma.Product {
	menuItems := []string{}
	productMap := make(map[string]ligma.Product)

	for _, p := range chem.Products {
		kn := p.ProductKey + " - " + p.Name
		menuItems = append(menuItems, kn)
		productMap[kn] = p
	}

	prompt := promptui.Select{
		Label: "Select product",
		Items: menuItems,
	}

	_, result, err := prompt.Run()

	fmt.Println(result)

	if err != nil {
		panic(err.Error())
	}

	return productMap[result]
}

func selectChemical(results ligma.SearchResult) (choice ligma.Chemical) {
	menuItems := []string{}
	chemMap := make(map[string]ligma.Chemical)

	for _, chem := range results.Chemicals {
		menuItems = append(menuItems, chem.Name)
		chemMap[chem.Name] = chem
	}

	prompt := promptui.Select{
		Label: fmt.Sprintf("%d results for \"%s\", select substance", results.SetsCount, results.SearchQuery),
		Items: menuItems,
	}

	_, result, err := prompt.Run()

	fmt.Println(result)

	if err != nil {
		panic(err.Error())
	}

	return chemMap[result]
}

func promptSearchQuery() (query string) {
	prompt := promptui.Prompt{
		Label: "Search Sigma",
		Validate: func(s string) error {
			if len(s) == 0 {
				return errors.New("enter a query")
			}
			return nil
		},
	}

	query, err := prompt.Run()

	if err != nil {
		panic(err)
	}

	return query
}

func performSearch() {
	searchResult := ligma.Search(strings.Join(flag.Args(), " "))

	if *jsonOutput {
		jsonString, err := json.Marshal(searchResult)
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			panic(err)
		}
		fmt.Println(string(jsonString))
	} else {
		if searchResult.SetsCount == 0 {
			fmt.Println("No search results found!")
			return
		}
		fmt.Println()
		fmt.Println(searchResult)
	}
}

func performScrape() {
	results := make([]ligma.ChemData, 0)

	var wg sync.WaitGroup

	for _, url := range flag.Args() {
		wg.Add(1)
		url := url
		go func() {
			results = append(results, ligma.Scrape(url))
			wg.Done()
		}()
	}

	wg.Wait()

	if *jsonOutput {
		jsonString, _ := json.Marshal(results)
		fmt.Print(string(jsonString))
		return
	}

	for _, chem := range results {
		printResults(chem)
	}
}

func printResults(booty ligma.ChemData) {
	booty.PrintName()
	fmt.Println()
	booty.PrintPictograms()
	booty.PrintParams()
	fmt.Println()
	booty.PrintHStatements()
	fmt.Println()
	booty.PrintPStatements()
	fmt.Println()
	booty.PrintConsolidatedHP()
}
