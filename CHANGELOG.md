# Changelog
## 0.1.2
- Cleaned up code a bit
- Overhauled CI for releases
- Added first result fetch mode

## 0.1.1
- Fixed search being broken due to Sigma changing their API
- Added JSON output to search
- Fixed false pictogram mappings
- Fixed some text

## 0.1.0
- Initial release