# Ligma Scraper CLI

## Index

- [Ligma Scraper CLI](#ligma-scraper-cli)
    - [Overview](#overview)
    - [Usage](#usage)
    - [How to get](#how-to-get)
    - [Contributing to the project](#contributing-to-the-project)
    - [Authors](#authors)

## Overview
A command line interface version of a tool of the same name that used to be on my website. It connects to https://www.sigmaaldrich.com allowing you to search for a chemical and pull safety- and other relefant information off their site, in order to make labelling chemicals in the hobby lab less tedious.

## Usage

### Interactive mode
```Shell
ligmascraper
```

### Search mode
```Shell
ligmascraper -S <your search query>
```

### Scrape mode
```Shell
ligmascraper -c <product page URLs separated by spaces>
```

### Scrape first result mode
```Shell
ligmascraper -c <your search query>
```

### JSON output
use the `-j` flag for non-interactive modes

## How to get
### Build from source
#### Preparation
- Make sure you have [go 1.19](https://golang.org) (or newer) available
- `cd` to the projects root directory
- Run `go get ./...` to ensure all the dependencies are available

#### Building
##### Quick way
- Just run `go run main.go` - this will compile it followed by running it in interactive mode
##### Proper way
- Run `go build -o bin/ligmascraper main.go` on a linux machine or `go build -o bin/ligmascraper.exe main.go` on a
  Windows machine.
- Either put the binary you just compiled somewhere better where its being able to be located by $PATH or add its location to the variable
- run it

#### Grab a binary
This repo is set up with automatic CI, that will build binaries every commit, you can grab them
from [the relevant release page](https://gitlab.com/ligmascraper/ligmascraper-cli/-/releases)

## Contributing to the project

### Via forking

- create an issue and discuss (optional, but encouraged)
- fork the project
- Make changes
- submit merge request

### Request developer access

- Request access
- create an issue and discuss (optional, but encouraged)
- Create a new branch
- Make changes
- Submit merge request

## Authors
Conceived and built by Holger M Mürk



