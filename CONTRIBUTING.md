# Contributing to the project

## Via forking

- create an issue and discuss (optional, but encouraged)
- fork the project
- Make changes
- submit merge request

## Request developer access

- Request access
- create an issue and discuss (optional, but encouraged)
- Create a new branch
- Make changes
- Submit merge request